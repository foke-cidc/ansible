# Ansible Web Server Deployment

This project uses Ansible and GitLab CI to automate the deployment of a simple HTML page on an Apache web server.

## Project Structure

The main components of this project are:
- `.gitlab-ci.yml`: Defines the CI/CD pipeline for deploying the application.
- `main.yml`: The Ansible playbook that configures the web server.
- `group_vars/vars.yml`: Contains variables used in the Ansible playbook.
- `inventory`: Defines the target hosts for Ansible.
- `ansible.cfg`: Configures Ansible behavior.

## Ansible Configuration

The `ansible.cfg` file sets the following configurations:

- `remote_user = afoke`: Specifies the remote user for SSH connections.
- `host_key_checking = false`: Disables SSH host key checking.
- `inventory = inventory`: Sets the inventory file location.
- `become = true`: Enables privilege escalation.
- `become_method = sudo`: Uses sudo for privilege escalation.
- `become_user = root`: Escalates privileges to root.
- `become_ask_pass = false`: Doesn't prompt for a password for privilege escalation.

## Ansible Playbook

The `main.yml` playbook performs the following tasks:

1. Installs Apache web server
2. Starts and enables the Apache service
3. Opens the firewall for HTTP traffic
4. Deploys a simple HTML page

### Target Hosts

The playbook targets hosts in the `webserver` group, which is defined in the `inventory` file:

- Host: `ansiblee1`
- IP: `192.168.116.139`

### Variables

Variables are defined in `group_vars/vars.yml`:
- `packages`: A list of packages to install
  - Currently includes: `httpd` (Apache web server)

## CI/CD Pipeline

The pipeline consists of two stages:

1. **Validate**: Checks the syntax of the Ansible playbook.
2. **Deploy**: Deploys the application to a production server.

### Validation Stage

- Uses the `local2` runner
- Installs the `ansible.posix` collection
- Checks the syntax of `main.yml` playbook

### Deployment Stage

- Uses the `local2` runner
- Deploys to a webserver environment
- Uses SSH to connect to the server and run the Ansible playbook

## Configuration

- Ansible user: `afoke`
- Server IP: `192.168.116.139`
- Server hostname: `ansiblee1`
- Web application URL: `http://192.168.116.139`

## Prerequisites

- GitLab runner with `local2` tag
- Ansible installed on both the GitLab runner and the target server
- SSH access to the target server
- Firewalld service
- Sudo access on the target server (as configured in `ansible.cfg`)

## Usage

1. Ensure all prerequisites are met.
2. Verify that `group_vars/vars.yml` contains the necessary packages (currently only `httpd`).
3. Confirm that the `inventory` file contains the correct host information.
4. Ensure `ansible.cfg` is properly configured for your environment.
5. Place your HTML content in `/index.html` on the GitLab runner.
6. Push changes to the repository to trigger the CI/CD pipeline.

The pipeline will validate the Ansible playbook and deploy it to the production server, setting up the Apache web server and deploying your HTML page.

## Files

- `main.yml`: The main Ansible playbook
- `.gitlab-ci.yml`: GitLab CI configuration file
- `group_vars/vars.yml`: Variables file for the Ansible playbook
- `inventory`: Ansible inventory file defining the target hosts
- `ansible.cfg`: Ansible configuration file
- `/index.html`: Source HTML file to be deployed

## Note

Ensure that the `SSH_PRIVATE_KEY` variable is set in your GitLab CI/CD settings for secure SSH access to the server.

## Customization

- To install additional packages, add them to the `packages` list in `group_vars/vars.yml`.
- To deploy to additional servers, add them to the `webserver` group in the `inventory` file.
- Modify `ansible.cfg` if you need to change Ansible's behavior, such as the remote user or privilege escalation settings.